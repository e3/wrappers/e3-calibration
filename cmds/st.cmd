# This should be a test startup script
epicsEnvSet "IOCNAME" "CalibIOC"
require calibration

epicsEnvSet("EssName", "001")
epicsEnvSet("AssetId", "999")

iocshLoad("$(calibration_DIR)/calibration.iocsh")
